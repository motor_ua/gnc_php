<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\AuthRequest;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use JWTAuth;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller
{
    /**
     * @SWG\Post(
     *   path="/api/authenticate",
     *   tags={"authenticate"},
     *   operationId="authenticate",
     *   summary="API Authenticate",
     *   @SWG\Parameter(
     *     name="email",
     * 	   in="formData",
     * 	   required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="password",
     * 	   in="formData",
     * 	   required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(
     *     response=201,
     *     description="successful operation",
     *     @SWG\Schema(ref="#/definitions/User"),
     *   ),
     *   @SWG\Response(
     *     response=422,
     *     description="Auth failed",
     *   ),
     *   @SWG\Response(
     *     response=400,
     *     description="Bad request",
     *   ),
     * )
     *
     * @param AuthRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function authenticate(AuthRequest $request)
    {
        $user = User::getByEmail($request->email);
        $token = $this->generateAuthToken($user);

        if (isset($token['error'])) {
            return response()->json(['error' => $token['error']], $token['status']);
        } else {
            return response()->json(['token' => $token], Response::HTTP_OK);
        }
    }

    /**
     * @param $user
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function generateAuthToken($user)
    {
        if(!$user){
            return ['error' => 'invalid_credentials','status' => 401];
        }

       // $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if ( !$token = $token = JWTAuth::fromUser($user)) {
                return response()->json(['error' => 'invalid_credentials'], Response::HTTP_UNAUTHORIZED);
            }
       /*     // attempt to verify the credentials and create a token for the current user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], Response::HTTP_UNAUTHORIZED);
            }*/
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        Cache::put($token, $user->id, config('jwt.ttl') ?: 60);

        // all good so return the token
        return $token;
    }
}

