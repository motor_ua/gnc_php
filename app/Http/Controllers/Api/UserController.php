<?php namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ApiController;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDateTime;

class UserController extends ApiController {

    /**
     * @SWG\Post(
     *   path="/api/user",
     *   tags={"Users"},
     *   operationId="registration",
     *   summary="User registration",
     *   @SWG\Parameter(
     *     name="sponsor",
     *     in="formData",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="username",
     * 	   in="formData",
     * 	   required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="email",
     * 	   in="formData",
     * 	   required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="fname",
     * 	   in="formData",
     * 	   required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="sname",
     *     in="formData",
     * 	   required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="phone",
     * 	   in="formData",
     * 	   required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="country",
     *     in="formData",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="password",
     *     in="formData",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="finPassword",
     *     in="formData",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="skype",
     *     in="formData",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="phoneViber",
     *     in="formData",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="phoneWhatsApp",
     *     in="formData",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="phoneTelegram",
     *     in="formData",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="phoneFB",
     *     in="formData",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="Success",
     *   ),
     *   @SWG\Response(
     *     response=400,
     *     description="Bad request",
     *   ),
     *   @SWG\Response(
     *     response=404,
     *     description="Sponsor not found",
     *   ),
     *   @SWG\Response(
     *     response=409,
     *     description="Login or email already exists",
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Other error",
     *   ),
     * )
     */
    public function register(Request $request)
    {
        $requestParams = $request->all();

        $validator = Validator::make($requestParams, [
            'sponsor' => 'required',
            'username' => 'required',
            'email' => 'required',
            'fname' => 'required',
            'sname' => 'required',
            'phone' => 'required',
            'password' => 'required',
            'finPassword' => 'required'
        ]);

        if ($validator->fails()) {
            return Response(['error' => $validator->errors()], Response::HTTP_BAD_REQUEST);
        } else {
            $sponsor = User::orWhere('username', '=', $requestParams['sponsor'])
                ->orWhere('email', '=', $requestParams['sponsor'])
                ->orWhere('accountId', '=', $requestParams['sponsor'])
                ->first();

            if (! $sponsor) {
                return Response(['error' => 'Sponsor not found'], Response::HTTP_NOT_FOUND);
            } else {
                return $this->_register($sponsor, $requestParams);
            }
        }
    }

    /**
     * @SWG\Get(
     *   path="/api/user/{param}",
     *   tags={"Users"},
     *   operationId="user_info",
     *   summary="User information",
     *   @SWG\Parameter(
     *     name="param",
     *     in="path",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="Success",
     *   ),
     *   @SWG\Response(
     *     response=400,
     *     description="Bad request",
     *   ),
     *   @SWG\Response(
     *     response=404,
     *     description="User not found",
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Other error",
     *   ),
     * )
     */
    public function userInfo($param = null)
    {
        if (! $param) {
            return Response(['error' => 'Bad request'], Response::HTTP_BAD_REQUEST);
        } else {
            $user = User::find($param);

            if (!$user) {
                if (!strstr($param, '+')) {
                    $paramWithPlus = '+' . $param;
                } else {
                    $paramWithPlus = $param;
                }

                $user = User::orWhere('username', '=', $param)
                    ->orWhere('accountId', '=', intval($param))
                    ->orWhere('email', '=', $param)
                    ->orWhere('phoneNumber', '=', $param)
                    ->orWhere('phoneNumber', '=', $paramWithPlus)
                    ->orWhere('phoneNumber2', '=', $param)
                    ->orWhere('phoneNumber2', '=', $paramWithPlus)
                    ->orWhere('phoneWellness', '=', $param)
                    ->orWhere('phoneWellness', '=', $paramWithPlus)
                    ->first();
            }

            if ($user) {
                return Response($user, Response::HTTP_OK);
            } else {
                return Response(['error' => 'User not found'], Response::HTTP_NOT_FOUND);
            }
        }
    }

    /**
     * @SWG\Post(
     *   path="/api/user/mobileRegistration",
     *   tags={"Users"},
     *   operationId="mobileRegistration",
     *   summary="User registration for mobile",
     *   @SWG\Parameter(
     *     name="sponsor_phone",
     *     in="formData",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="phone",
     * 	   in="formData",
     * 	   required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="email",
     * 	   in="formData",
     * 	   required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="country",
     *     in="formData",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="password",
     *     in="formData",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="Success",
     *   ),
     *   @SWG\Response(
     *     response=400,
     *     description="Bad request",
     *   ),
     *   @SWG\Response(
     *     response=404,
     *     description="Sponsor not found",
     *   ),
     *   @SWG\Response(
     *     response=409,
     *     description="Login or email already exists",
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Other error",
     *   ),
     * )
     */
    public function mobileRegister(Request $request)
    {
        $requestParams = $request->all();

        $validator = Validator::make($requestParams, [
            'sponsor_phone' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'country' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return Response(['error' => $validator->errors()], Response::HTTP_BAD_REQUEST);
        } else {
            $sponsorPhone = str_replace('+', '', $requestParams['sponsor_phone']);
            $email = $requestParams['email'];
            $phone = str_replace('+', '', $requestParams['phone']);

            if (User::orWhere('phoneNumber', '=', $phone)
                ->orWhere('phoneNumber', '=', '+' . $phone)
                ->orWhere('phoneNumber2', '=', $phone)
                ->orWhere('phoneNumber2', '=', '+' . $phone)
                ->orWhere('phoneWellness', '=', $phone)
                ->orWhere('phoneWellness', '=', '+' . $phone)
                ->orWhere('email', '=', $email)
                ->first())
            {
                return Response(['error' => 'User already exists'], Response::HTTP_NOT_FOUND);
            } else {
                if (! $sponsor = User::orWhere('phoneNumber', '=', $sponsorPhone)
                    ->orWhere('phoneNumber', '=', '+' . $sponsorPhone)
                    ->orWhere('phoneNumber2', '=', $sponsorPhone)
                    ->orWhere('phoneNumber2', '=', '+' . $sponsorPhone)
                    ->orWhere('phoneWellness', '=', $sponsorPhone)
                    ->orWhere('phoneWellness', '=', '+' . $sponsorPhone)
                    ->first()
                ) {
                    return Response(['error' => 'Sponsor not found'], Response::HTTP_NOT_FOUND);
                } else {
                    return $this->_register($sponsor, $requestParams);
                }
            }
        }
    }

    /**
     * @param User $sponsor
     * @param $requestParams
     * @return Response
     */
    private function _register(User $sponsor, $requestParams)
    {
        if ($sponsor->settings['manualRegistrationControl'] == 0 || ! $sponsor->nextRegistration) {
            $lastUser = $sponsor->getLastUser($sponsor->sideToNextUser);
        } else {
            $nextRegistrationUser = User::find($sponsor->nextRegistration['_id']);

            if ($nextRegistrationUser) {
                $lastUser = $nextRegistrationUser->getLastUser($sponsor->sideToNextUser);
            }
        }

        if (isset($lastUser) && $lastUser) {
            $user = new User();

            $sponsorShortData = [
                '_id' => new ObjectID($sponsor->_id),
                'email' => $sponsor->email,
                'skype' => $sponsor->skype,
                'username' => $sponsor->username,
                'accountId' => $sponsor->accountId,
                'avatar' => $sponsor->avatar,
                'rank' => $sponsor->rank,
                'links' => $sponsor->links,
                'zipCode' => $sponsor->zipCode,
                'state' => $sponsor->state,
                'country' => $sponsor->country,
                'city' => $sponsor->city,
                'address' => $sponsor->address,
                'sideToNextUser' => $sponsor->sideToNextUser,
                'phoneNumber2' => $sponsor->phoneNumber2,
                'phoneNumber' => $sponsor->phoneNumber,
                'firstName' => $sponsor->firstName,
                'secondName' => $sponsor->secondName,
                'cards' => $sponsor->cards
            ];

            $user->sponsorId = new ObjectID($sponsor->_id);
            $user->email = $requestParams['email'];
            $user->username = isset($requestParams['username']) ? $requestParams['username'] : '';
            $user->phoneNumber = $requestParams['phone'];
            $user->country = $requestParams['country'];
            $user->parentId = new ObjectID($lastUser->_id);
            $user->accountId = intval(rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9));
            $user->sponsor = $sponsorShortData;
            $user->sideToNextUser = $sponsor->sideToNextUser;
            $user->side = $sponsor->sideToNextUser;
            $user->moneys = 0;
            $user->pointsLeft = 0;
            $user->pointsRight = 0;
            $user->created = new UTCDateTime(time() * 1000);
            $user->cardNumber = '';
            $user->phoneNumber2 = '';
            $user->phoneWellness = '';
            $user->bs = false;
            $user->qualification = false;
            $user->defaultLang = '';
            $user->address = '';
            $user->city = '';
            $user->state = '';
            $user->zipCode = '';
            $user->isAdmin = 0;
            $user->personal = false;
            $user->leftSideNumberUsers = 0;
            $user->rightSideNumberUsers = 0;
            $user->status = 1;
            $user->avatar = '';
            $user->rank = 0;
            $user->structBonus = false;
            $user->personalBonus = false;
            $user->autoExtensionBS = false;
            $user->linkedAccounts = [];
            $user->careerHistory = [];

            $system = [
                'readNews' => [],
                'readPromotions' => []
            ];

            $user->system = $system;

            $warehouseName = [
                'ru' => '',
                'en' => ''
            ];

            $user->warehouseName = $warehouseName;

            $landing = [
                'analytics' => '',
                'analytics2' => '',
                'analytics_vipvip' => '',
                'analytics_webwellness_ru' => '',
                'analytics_webwellness_net' => ''
            ];

            $user->landing = $landing;

            $user->potential = 0;

            $links = [
                'site' => '',
                'odnoklassniki' => '',
                'vk' => '',
                'fb' => '',
                'youtube' => ''
            ];

            $user->links = $links;

            $statistics = [
                'partnersWithPurchases' => 0,
                'structIncome' => 0,
                'personalIncome' => 0,
                'personalPartners' => 0,
                'personalPartnersWithPurchases' => 0,
                'unreadNews' => 0,
                'unreadPromotions' => 0,
                'steps' => 0,
                'pack' => 0,
                'mentorBonus' => 0,
                'careerBonus' => 0,
                'executiveBonus' => 0,
                'worldBonus' => 0,
                'autoBonus' => 0,
                'propertyBonus' => 0,
                'priorityInvestmentBonus' => 0,
                'sharesVIPVIP' => 0,
                'dividendsVIPVIP' => 0,
                'tokens' => 0,
                'stock' => [
                    'vipvip' => [
                        'total' => 0,
                        'buy' => 0,
                        'earned' => 0
                    ],
                    'wellness' => [
                        'total' => 0,
                        'buy' => 0,
                        'earned' => 0
                    ]
                ],
            ];

            $user->statistics = $statistics;

            $settings = [
                'layout' => 1,
                'showMobile' => 1,
                'showEmail' => 1,
                'showName' => 1,
                'deliveryEMail' => 1,
                'deliverySMS' => 1,
                'notifyAboutCheck' => 1,
                'notifyAboutJoinPartner' => 1,
                'notifyAboutReceiptsMoney' => 1,
                'notifyAboutReceiptsPoints' => 1,
                'notifyAboutEndActivity' => 1,
                'notifyAboutOtherNews' => 1,
                'selectedLang' => 'ru',
                'charityPercent' => 0,
                'manualRegistrationControl' => 0,
                'phoneWhatsApp' => '',
                'phoneViber' => '',
                'phoneTelegram' => '',
                'phoneFB' => '',
                'onMapX' => '',
                'onMapY' => ''
            ];

            $user->settings = $settings;

            if (isset($requestParams['skype']) && $requestParams['skype']) {
                $user->skype = $requestParams['skype'];
            }

            if (isset($requestParams['phoneViber']) && $requestParams['phoneViber']) {
                $user->setAttribute('settings.phoneViber', $requestParams['phoneViber']);
            }

            if (isset($requestParams['phoneTelegram']) && $requestParams['phoneTelegram']) {
                $user->setAttribute('settings.phoneTelegram', $requestParams['phoneTelegram']);
            }

            if (isset($requestParams['phoneWhatsApp']) && $requestParams['phoneWhatsApp']) {
                $user->setAttribute('settings.phoneWhatsApp', $requestParams['phoneWhatsApp']);
            }

            if (isset($requestParams['phoneFB']) && $requestParams['phoneFB']) {
                $user->setAttribute('settings.phoneFB', $requestParams['phoneFB']);
            }

            if ($user->save()) {
                $user->_plainPassword = $requestParams['password'];
                $user->hashedPassword = hash_hmac('sha1', $requestParams['password'], $user->_id);
                $user->salt = $user->_id;
                if (isset($requestParams['finPassword'])) {
                    $user->_plainFinPassword = $requestParams['finPassword'];
                    $user->hashedFinPassword = hash_hmac('sha1', $requestParams['finPassword'], $user->_id);
                    $user->finSalt = $user->_id;
                }
                if ($user->save()) {
                    switch ($sponsor->sideToNextUser) {
                        case User::SIDE_LEFT:
                            $lastUser->chldrnLeftId = new ObjectID($user->_id);
                            break;
                        case User::SIDE_RIGHT:
                            $lastUser->chldrnRightId = new ObjectID($user->_id);
                            break;
                    }

                    if ($lastUser->save() && $sponsor->settings['manualRegistrationControl'] == 0) {
                        $nextRegistrationShortData = [
                            '_id' => new ObjectID($user->_id),
                            'email' => $user->email,
                            'skype' => $user->skype,
                            'username' => $user->username,
                            'accountId' => $user->accountId,
                            'avatar' => $user->avatar,
                            'rank' => $user->rank,
                            'links' => $user->links,
                            'zipCode' => $user->zipCode,
                            'state' => $user->state,
                            'country' => $user->country,
                            'city' => $user->city,
                            'address' => $user->address,
                            'sideToNextUser' => $user->sideToNextUser,
                            'phoneNumber2' => $user->phoneNumber2,
                            'phoneNumber' => $user->phoneNumber,
                            'firstName' => $user->firstName,
                            'secondName' => $user->secondName,
                            'cards' => $user->cards
                        ];

                        $sponsor->nextRegistration = $nextRegistrationShortData;

                        if ($sponsor->save()) {
                            /**
                             * @todo add timezone, stock for registration, email for registration
                             */
                            return Response('', Response::HTTP_OK);
                        } else {
                            return Response(['error' => 'Sponsor next registration not updated'], Response::HTTP_INTERNAL_SERVER_ERROR);
                        }
                    }
                } else {
                    return Response(['error' => 'User password not saved'], Response::HTTP_INTERNAL_SERVER_ERROR);
                }
            } else {
                return Response(['error' => 'User not created'], Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        } else {
            return Response(['error' => 'Last user not found'], Response::HTTP_NOT_FOUND);
        }
    }

}