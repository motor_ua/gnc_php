<?php namespace App\Http\Controllers\Api;

use App\Events\SaleCanceled;
use App\Events\SaleCreated;
use App\Models\Sale;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Pin;
use App\Models\Product;
use MongoDB\BSON\UTCDateTime;

class SaleController extends ApiController {

    /**
     * @SWG\Post(
     *   path="/api/sales",
     *   tags={"Sales"},
     *   operationId="createSale",
     *   summary="Create a sale",
     *   @SWG\Parameter(
     *     name="pin",
     * 	   in="formData",
     * 	   required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="phone",
     * 	   in="formData",
     * 	   required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="iduser",
     * 	   in="formData",
     * 	   required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="project",
     * 	   in="formData",
     * 	   required=false,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="warehouse",
     * 	   in="formData",
     * 	   required=false,
     *     type="string"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="Success",
     *   ),
     *   @SWG\Response(
     *     response=400,
     *     description="Bad request",
     *   ),
     *   @SWG\Response(
     *     response=406,
     *     description="Pin used",
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Other error",
     *   ),
     * )
     */
    public function createSale(Request $request)
    {
        $requestParams = $request->all();

        $validator = Validator::make($requestParams, [
            'pin' => 'required'
        ]);

        if ($validator->fails()) {
            return Response(['error' => $validator->errors()], Response::HTTP_BAD_REQUEST);
        } else {
            if ($pin = Pin::where('pin', '=', $requestParams['pin'])->first()) {
                $project = isset($requestParams['project']) && $requestParams['project'] ? $requestParams['project'] : 2;
                $isBPT = $project == 1;

                if (($isBPT && $pin->used) || $pin->isActivate) {
                    return Response(['error' => 'Pin used'], Response::HTTP_NOT_ACCEPTABLE);
                } else {
                    if (isset($requestParams['iduser']) && $requestParams['iduser']) {
                        $user = User::find($requestParams['iduser']);
                    } else if (isset($requestParams['phone']) && $requestParams['phone']) {
                        if ($requestParams['phone'][0] != '+') {
                            $phone = '+' . $requestParams['phone'];
                        } else {
                            $phone = $requestParams['phone'];
                        }
                        $user = User::where('phoneNumber', '=', $phone)->orWhere('phoneNumber2', '=', $phone)->orWhere('phoneWellness', '=', $phone)->first();
                    }
                    if ($user) {
                        $pinInfo = $pin->getRepository()->decrypt();

                        if ($pinInfo) {
                            $product = Product::where('idInMarket', '=', $pinInfo['idInMarket'])->first();

                            if ($product) {
                                if ($isBPT && ($product->type == 3 || $product->type == 7)) {
                                    return Response(['error' => 'Pin from other project'], Response::HTTP_INTERNAL_SERVER_ERROR);
                                } else {
                                    if ($pin->used) {
                                        $pin->isActivate = true;
                                        if ($pin->update()) {
                                            return Response('', Response::HTTP_OK);
                                        } else {
                                            return Response(['error' => 'Pin not updated'], Response::HTTP_INTERNAL_SERVER_ERROR);
                                        }
                                    } else {
                                        if (isset($requestParams['warehouse'])) {
                                            if (User::find($requestParams['warehouse'])) {
                                                $warehouseId = $requestParams['warehouse'];
                                            } else {
                                                return Response(['error' => 'Warehouse not found'], Response::HTTP_INTERNAL_SERVER_ERROR);
                                            }
                                        }
                                        $sale = new Sale();
                                        $sale->idUser      = new \MongoDB\BSON\ObjectID($user->_id);
                                        $sale->product     = $product->product;
                                        $sale->productName = $product->productName;
                                        $sale->username    = $user->username;
                                        $sale->productType = $product->type;
                                        $sale->project     = $project;
                                        $sale->price       = $product->price;
                                        $sale->bonusMoney  = $product->bonusMoney;
                                        $sale->bonusPoints = $product->bonusPoints;
                                        $sale->bonusStocks = $product->bonusStocks;
                                        $sale->type        = Sale::TYPE_CREATED;
                                        $sale->reduced     = false;
                                        $sale->dateCreate  = new UTCDateTime(time() * 1000);
                                        if (isset($warehouseId) && $warehouseId) {
                                            $sale->warehouseId = new \MongoDB\BSON\ObjectID($warehouseId);
                                        }
                                        if ($sale->save()) {
                                            $pin->userId = new \MongoDB\BSON\ObjectID($user->_id);
                                            $pin->isActivate = $isBPT ? false : true;
                                            $pin->used = $isBPT ? true : false;
                                            if ($pin->update()) {
                                                event(new SaleCreated($sale, $pin));
                                                return Response('', Response::HTTP_OK);
                                            } else {
                                                return Response(['error' => 'Pin not updated'], Response::HTTP_INTERNAL_SERVER_ERROR);
                                            }
                                        } else {
                                            return Response(['error' => 'Sale not created'], Response::HTTP_INTERNAL_SERVER_ERROR);
                                        }
                                    }
                                }
                            } else {
                                return Response(['error' => 'Product not found'], Response::HTTP_NOT_FOUND);
                            }
                        } else {
                            return Response(['error' => 'Pin incorrect'], Response::HTTP_INTERNAL_SERVER_ERROR);
                        }
                    } else {
                        return Response(['error' => 'User not found'], Response::HTTP_NOT_FOUND);
                    }
                }
            } else {
                return Response(['error' => 'Pin not found'], Response::HTTP_NOT_FOUND);
            }
        }
    }

    /**
     * @SWG\Delete(
     *   path="/api/sales",
     *   tags={"Sales"},
     *   operationId="cancelSale",
     *   summary="Cancel a sale",
     *   @SWG\Parameter(
     *     name="id",
     * 	   in="formData",
     * 	   required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="Success",
     *   ),
     *   @SWG\Response(
     *     response=404,
     *     description="Sale not found",
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Other error",
     *   ),
     * )
     */
    public function cancelSale(Request $request)
    {
        $requestParams = $request->all();

        $validator = Validator::make($requestParams, [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return Response(['error' => $validator->errors()], Response::HTTP_BAD_REQUEST);
        } else {
            if ($sale = Sale::find($requestParams['id'])) {
                if ($sale->type == Sale::TYPE_CREATED && $sale->reduced) {
                    $sale->reduced = false;
                    $sale->type = Sale::TYPE_CANCELED;
                    if ($sale->save()) {
                        event(new SaleCanceled($sale));
                        return Response('', Response::HTTP_OK);
                    } else {
                        return Response(['error' => 'Sale not canceled'], Response::HTTP_INTERNAL_SERVER_ERROR);
                    }
                } else {
                    return Response(['error' => 'Sale already canceled'], Response::HTTP_INTERNAL_SERVER_ERROR);
                }
            } else {
                return Response(['error' => 'Sale not found'], Response::HTTP_NOT_FOUND);
            }
        }
    }

}