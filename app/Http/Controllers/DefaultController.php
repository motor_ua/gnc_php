<?php

namespace App\Http\Controllers;

use App\Events\AutoBonusAdded;
use App\Events\MentorBonusSetted;
use App\Models\Sale;
use App\Models\Settings;
use App\Models\User;
use App\Models\News;
use App\Models\Promotion;
use App\Models\Transaction;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use MongoDB\BSON\UTCDateTime;
use MongoDB\BSON\ObjectID;
use App\Models\Career;
use MongoDB\Driver\Exception\InvalidArgumentException;

class DefaultController extends Controller
{
    public function index()
    {
        $user = User::find('573a0d76965dd0fb16f60bfe');

        $start = microtime(true);
        echo $user->getRepository()->countPartners(User::SIDE_LEFT) . ' ';
        $time = microtime(true) - $start;
        printf('Скрипт LEFT выполнялся %.4F сек.', $time);

        echo '</br>';

        $start = microtime(true);
        echo $user->getRepository()->countPartners(User::SIDE_RIGHT) . ' ';
        $time = microtime(true) - $start;
        printf('Скрипт RIGHT выполнялся %.4F сек.', $time);

        echo '</br>';

        $start = microtime(true);
        echo $user->getRepository()->countPartnersWithPurchases() . ' ';
        $time = microtime(true) - $start;
        printf('Скрипт выполнялся %.4F сек.', $time);
    }

}