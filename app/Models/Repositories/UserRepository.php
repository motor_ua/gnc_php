<?php

namespace App\Models\Repositories;

use App\Models\Sale;
use App\Models\User;
use MongoDB\BSON\UTCDateTime;
use MongoDB\BSON\ObjectID;

class UserRepository
{
    public $model;

    public function __construct(User $user)
    {
        $this->model = $user;

        return $this;
    }

    public function getUsersToMain()
    {
        $users = [];
        $this->_getParent($this->model, $users);

        return $users;
    }

    /**
     * @param User $user
     * @param $result
     * @return array
     */
    private function _getParent(User $user, &$result)
    {
        $parent = $user->parent;

        if (!$parent) {
            return $result;
        } else {
            $result[] = $parent;
            $this->_getParent($parent, $result);
        }
    }

    /**
     * @return bool
     */
    public function havePack()
    {
        return $this->model->pack >= User::PACK_BEGINNER && $this->model->pack <= User::PACK_VIP;
    }

    /**
     * @return mixed
     */
    public function getFirstSale()
    {
        return $firstSale = Sale::where('idUser', '=', new ObjectID($this->model->_id))
            ->where('reduced', '=', true)
            ->where('type', '=', Sale::TYPE_CREATED)
            ->orderBy('dateReduce', 'asc')->first();
    }

    /**
     * @param $limit
     * @param $offset
     * @return mixed
     */
    public function getPersonalPartners($limit, $offset)
    {
        return User::where('sponsorId', '=', new ObjectID($this->model->_id))->limit($limit)->offset($offset)->get();
    }

    /**
     * @return mixed
     */
    public function getPersonalPartnersCount()
    {
        return User::count('sponsorId', '=', new ObjectID($this->model->_id));
    }

    /**
     * @param $side
     * @return int
     */
    public function countPartners($side)
    {
        $count = 0;

        switch ($side) {
            case User::SIDE_LEFT:
                if ($this->model->chldrnLeftId) {
                    $user = User::where('_id', '=', new ObjectID($this->model->chldrnLeftId))->select('chldrnLeftId', 'chldrnRightId')->first();
                }
                if (isset($user) && $user) {
                    $count++;
                    $this->_countPartners([$user], $count);
                }
                break;
            case User::SIDE_RIGHT:
                if ($this->model->chldrnRightId) {
                    $user = User::where('_id', '=', new ObjectID($this->model->chldrnRightId))->select('chldrnLeftId', 'chldrnRightId')->first();
                }
                if (isset($user) && $user) {
                    $count++;
                    $this->_countPartners([$user], $count);
                }
                break;
        }

        return $count;
    }

    /**
     * @param $users
     * @param $number
     */
    private function _countPartners($users, &$number)
    {
        if (!$users) {
            return;
        } else {
            $tmpUsers = [];
            foreach ($users as $user) {
                if ($user->chldrnLeftId) {
                    $number++;
                    $tmpUsers[] = User::where('_id', '=', new ObjectID($user->chldrnLeftId))->select('chldrnLeftId', 'chldrnRightId')->first();
                }
                if ($user->chldrnRightId) {
                    $number++;
                    $tmpUsers[] = User::where('_id', '=', new ObjectID($user->chldrnRightId))->select('chldrnLeftId', 'chldrnRightId')->first();
                }
            }
            $this->_countPartners($tmpUsers, $number);
        }
    }

    public function countPartnersWithPurchases()
    {
        $count = 0;

        $this->_countPartnersWithPurchases([$this->model], $count);

        return $count;
    }

    private function _countPartnersWithPurchases($users, &$number)
    {
        if (!$users) {
            return;
        } else {
            $tmpUsers = [];
            foreach ($users as $user) {
                if ($user->chldrnLeftId) {
                    $leftUser = User::where('_id', '=', new ObjectID($user->chldrnLeftId))->select('firstPurchase', 'chldrnLeftId', 'chldrnRightId')->first();
                    if ($leftUser->firstPurchase) {
                        if (is_string($leftUser->firstPurchase)) {
                            $firstSaleDate = strtotime($leftUser->firstPurchase);
                        } else {
                            $firstSaleDate = $leftUser->firstPurchase->toDateTime()->getTimestamp();
                        }
                        if ($firstSaleDate > 0) {
                            $number++;
                        }
                    }
                    $tmpUsers[] = $leftUser;
                }
                if ($user->chldrnRightId) {
                    $rightUser = User::where('_id', '=', new ObjectID($user->chldrnRightId))->select('firstPurchase', 'chldrnLeftId', 'chldrnRightId')->first();
                    if ($rightUser->firstPurchase) {
                        if (is_string($rightUser->firstPurchase)) {
                            $firstSaleDate = strtotime($rightUser->firstPurchase);
                        } else {
                            $firstSaleDate = $rightUser->firstPurchase->toDateTime()->getTimestamp();
                        }
                        if ($firstSaleDate > 0) {
                            $number++;
                        }
                    }
                    $tmpUsers[] = $rightUser;
                }
            }
            $this->_countPartnersWithPurchases($tmpUsers, $number);
        }
    }

    /**
     * @param $level
     * @return array
     */
    public function getPersonalSpilover($level)
    {
        $user = $this->model;

        $currentLevel = 0;

        $tmpUsers = [$user];
        $spilovers = [];

        while ($currentLevel < $level) {
            $tmpUsers2 = $tmpUsers;
            $tmpUsers = [];

            foreach ($tmpUsers2 as $tmpUser2) {
                $users = User::where('sponsorId', '=', new ObjectID($tmpUser2->_id))->get();

                foreach ($users as $u) {
                    array_push($spilovers, $u);
                    array_push($tmpUsers, $u);
                }
            }

            $currentLevel++;
        }

        return $spilovers;
    }

    /**
     * @param $side
     * @return mixed
     */
    public function getLastUser($side)
    {
        $result = '';

        $this->_getLastUser($this->model, $side, $result);

        return $result;
    }

    /**
     * @param User $user
     * @param $side
     * @return User
     */
    private function _getLastUser(User $user, $side, &$result)
    {
        switch ($side) {
            case User::SIDE_LEFT:
                if ($user->chldrnLeftId) {
                    $leftUser = User::find($user->chldrnLeftId);
                    if ($leftUser) {
                        $this->_getLastUser($leftUser, $side, $result);
                    } else {
                        $result = false;
                    }
                } else {
                    $result = $user;
                }
                break;
            case User::SIDE_RIGHT:
                if ($user->chldrnRightId) {
                    $rightUser = User::find($user->chldrnRightId);
                    if ($rightUser) {
                        $this->_getLastUser($rightUser, $side, $result);
                    } else {
                        $result = false;
                    }
                } else {
                    $result = $user;
                }
                break;
        }
    }

}