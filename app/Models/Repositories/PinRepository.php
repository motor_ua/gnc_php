<?php

namespace App\Models\Repositories;

use App\Models\Pin;
use App\Models\Settings;

class PinRepository {

    public $model;

    public function __construct(Pin $pin)
    {
        $this->model = $pin;

        return $this;
    }

    public function decrypt()
    {
        $key = Settings::first()->pinsKey;

        if ($key) {
            return $this->_parsePinInfo(mcrypt_decrypt(MCRYPT_BLOWFISH, md5($key, true), hex2bin($this->model->pin), MCRYPT_MODE_ECB));
        } else {
            /**
             * @todo throw Exception
             */
            return false;
        }
    }

    public function encrypt()
    {

    }

    private function _parsePinInfo($pinInfo)
    {
        $phpInfoArray = explode('|', $pinInfo);

        if (count($phpInfoArray) == 2) {
            $phpInfoIdInMarketAndNymber = explode('.', $phpInfoArray[0]);

            if (count($phpInfoIdInMarketAndNymber) == 2) {
                $result = [
                    'idInMarket' => (int)$phpInfoIdInMarketAndNymber[0],
                    'number'     => (int)$phpInfoIdInMarketAndNymber[1],
                    'dateCreate' => (int)((int)$phpInfoArray[1] / 1000)
                ];
            } else {
                $result = false;
            }
        } else {
            $result = false;
        }

        return $result;
    }
}