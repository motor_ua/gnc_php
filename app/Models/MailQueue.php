<?php

namespace App\Models;

use Moloquent;

class MailQueue extends Moloquent {

    const MESSENGER_WHATSAPP = 'Whatsapp';
    const MESSENGER_VIBER = 'Viber';
    const MESSENGER_FACEBOOK = 'Facebook';
    const MESSENGER_TELEGRAM = 'Telegram';
    const MESSENGERS = [self::MESSENGER_WHATSAPP, self::MESSENGER_VIBER, self::MESSENGER_FACEBOOK, self::MESSENGER_TELEGRAM];

}