<?php

namespace App\Models;

use Moloquent;

class User extends Moloquent {

//    protected $connection = 'mongodb';
//    protected $collection = 'users';
    protected $primaryKey = '_id';

    const PACK_BEGINNER = 1;
    const PACK_STANDART = 2;
    const PACK_VIP = 3;

    const SIDE_LEFT = 1;
    const SIDE_RIGHT = 0;

    /**
     * @param $phone
     * @return mixe
     * @todo Вынести содержимое в UserRepository
     */
    public static function getByPhone($phone)
    {
        return self::where('phoneNumber', '=', $phone)->orWhere('phoneNumber2', '=', $phone)->orWhere('phoneWellness', '=', $phone)->first();
    }

    /**
     * @param $email
     * @return mixed
     * @todo Вынести содержимое в UserRepository
     */
    public static function getByEmail($email)
    {
        return self::where('email', '=', $email)->first();
    }
    
    /**
     * @return mixed
     */
    public function sponsor()
    {
        return $this->belongsTo('App\Models\User', 'sponsorId', '_id');
    }

    /**
     * @return mixed
     */
    public function embedsSponsor()
    {
        return $this->embedsOne('App\Models\User', 'sponsor');
    }

    /**
     * @return mixed
     */
    public function parent()
    {
        return $this->belongsTo('App\Models\User', 'parentId', '_id');
    }

    /**
     * @return mixed
     */
    public function transactionsTo()
    {
        /**
         * @todo change to _id and idTo
         */
        return $this->hasMany('App\Models\Transaction', 'usernameTo', 'username');
    }

    /**
     * @return mixed
     */
    public function transactionsFrom()
    {
        /**
         * @todo change to _id and idFrom
         */
        return $this->hasMany('App\Models\Transaction', 'usernameFrom', 'username');
    }

    /**
     * @return mixed
     */
    public function sales()
    {
        /**
         * @todo change to _id and idUser
         */
        return $this->hasMany('App\Models\Sale', 'idUser', '_id');
    }

    /**
     * @return Repositories\UserRepository
     */
    public function getRepository()
    {
        return new Repositories\UserRepository($this);
    }

    /**
     * @param $side
     * @return mixed
     */
    public function getLastUser($side)
    {
        return $this->getRepository()->getLastUser($side);
    }
}