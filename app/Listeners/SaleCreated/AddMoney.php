<?php

namespace App\Listeners\SaleCreated;

use App\Events\MoneyAdded;
use App\Events\SaleCreated;
use App\Models\Sale;
use App\Models\Transaction;
use App\Models\User;

class AddMoney
{
    /**
     * @param SaleCreated $event
     */
    public function handle(SaleCreated $event)
    {
        $mainUser = User::where('username', '=', 'main')->first();

        $comment = 'Purchase for a partner ' . $event->sale->user->username;

        /**
         * Записываем приход средств за покупку, на счет основного пользователя.
         */
        if ($event->sale->productType !== Sale::PRODUCT_TYPE_BALANCE) {
            $transaction = Transaction::addMoneys($event->sale, $mainUser, $mainUser, $event->sale->price, $comment);

            if ($transaction) {
                event(new MoneyAdded($transaction));
            }
        }

        /**
         * Выплачиваем бонус за счет средств основного пользователя.
         */
        $user = $event->sale->user;
        $sponsor = $event->sale->user->sponsor;

        switch($event->sale->productType) {
            case Sale::PRODUCT_TYPE_VIPVIP:
            case Sale::PRODUCT_TYPE_WELLNESS:
                if ($sponsor->bs && $event->sale->bonusMoney > 0) {
                    $comment = 'Purchase for a partner ' . $event->sale->user->username;

                    $transaction = Transaction::addMoneys($event->sale, $mainUser, $sponsor, $event->sale->bonusMoney, $comment);

                    if ($transaction) {
                        event(new MoneyAdded($transaction));
                    }
                }
            break;
            case Sale::PRODUCT_TYPE_BALANCE_VIPVIP:
            case Sale::PRODUCT_TYPE_BALANCE_WELLNESS:
            case Sale::PRODUCT_TYPE_BALANCE_TOP_UP:
                if ($user->bs && $event->sale->bonusMoney > 0) {
                    $comment = 'Purchase for a partner ' . $event->sale->user->username;

                    $transaction = Transaction::addMoneys($event->sale, $mainUser, $user, $event->sale->bonusMoney, $comment);

                    if ($transaction) {
                        event(new MoneyAdded($transaction));
                    }
                }
            break;
            case Sale::PRODUCT_TYPE_BALANCE:
                $comment = 'Entering the money';

                $transaction = Transaction::addMoneys($event->sale, $mainUser, $user, $event->sale->price, $comment);

                if ($transaction) {
                    event(new MoneyAdded($transaction));
                }
            break;
        }
    }

}
