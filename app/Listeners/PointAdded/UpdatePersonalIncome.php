<?php

namespace App\Listeners\PointAdded;

use App\Events\PointAdded;

class UpdatePersonalIncome
{
    /**
     * @param PointAdded $event
     */
    public function handle(PointAdded $event)
    {
        $amount = $event->transaction->amount;

        $user = $event->transaction->userTo;

        $personalIncome = $user->statistics['personalIncome'] + $amount;

        $user->setAttribute('statistics.personalIncome', $personalIncome);

        $user->save();
    }

}
