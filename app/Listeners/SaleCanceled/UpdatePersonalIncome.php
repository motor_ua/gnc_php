<?php

namespace App\Listeners\SaleCanceled;

use App\Events\SaleCanceled;

class UpdatePersonalIncome
{
    /**
     * @param SaleCanceled $event
     */
    public function handle(SaleCanceled $event)
    {
        $amount = $event->sale->price;

        $user = $event->sale->user;

        $personalIncome = $user->statistics['personalIncome'] - $amount;

        $user->setAttribute('statistics.personalIncome', $personalIncome);

        $user->save();
    }

}
