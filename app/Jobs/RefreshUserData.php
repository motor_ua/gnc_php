<?php

namespace App\Jobs;

use App\Events\MoneyAdded;
use App\Events\SaleCreated;
use App\Models\Sale;
use App\Models\Transaction;
use App\Models\User;
use App\Models\News;
use App\Models\Product;
use App\Models\Promotion;
use Carbon\Carbon;
use MongoDB\BSON\UTCDateTime;
use MongoDB\BSON\ObjectID;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use MongoDB\Driver\Exception\InvalidArgumentException;

class RefreshUserData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const LIMIT = 100;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $usersNumber = User::count();

        $counter = intval(ceil($usersNumber / self::LIMIT));

        $c = 0;
        for ($i = 0; $i <= $counter; $i++) {
            $users = User::limit(self::LIMIT)->offset($i * self::LIMIT)->get();

            foreach ($users as $user) {
                ++$c;
                echo "\n USER $c/$usersNumber\n";

                echo "\n START: " . gmdate('d.m.Y H:i:s', time()) . ' ' . $user->username . "...\n";

                $user->personalBonus = $user->getRepository()->havePack() && $user->bs;

                $user->structBonus = $user->qualification && $user->bs;

                $sponsor = $user->sponsor;

                if ($sponsor) {
                    $sponsorShortData = [
                        '_id' => new ObjectID($sponsor->_id),
                        'email' => $sponsor->email,
                        'skype' => $sponsor->skype,
                        'username' => $sponsor->username,
                        'accountId' => $sponsor->accountId,
                        'avatar' => $sponsor->avatar,
                        'rank' => $sponsor->rank,
                        'links' => $sponsor->links,
                        'zipCode' => $sponsor->zipCode,
                        'state' => $sponsor->state,
                        'country' => $sponsor->country,
                        'city' => $sponsor->city,
                        'address' => $sponsor->address,
                        'sideToNextUser' => $sponsor->sideToNextUser,
                        'phoneNumber2' => $sponsor->phoneNumber2,
                        'phoneNumber' => $sponsor->phoneNumber,
                        'firstName' => $sponsor->firstName,
                        'secondName' => $sponsor->secondName,
                        'cards' => $sponsor->cards
                    ];
                    $user->sponsor = $sponsorShortData;
                }

                if ($user->firstPurchase) {
                    if (is_string($user->firstPurchase)) {
                        $firstSaleDate = strtotime($user->firstPurchase);
                    } else {
                        $firstSaleDate = $user->firstPurchase->toDateTime()->getTimestamp();
                    }
                    if ($firstSaleDate < 0) {
                        $firstSale = $user->getRepository()->getFirstSale();

                        if ($firstSale) {
                            $user->firstPurchase = $firstSale->dateReduce;
                        }
                    }
                }

                $personalPartnersCount = $user->getRepository()->getPersonalPartnersCount();

                $user->setAttribute('statistics.personalPartners', $personalPartnersCount);

                $personalPartnersWithPurchases = 0;

                $counterPersonalPartners = intval(ceil($personalPartnersCount / self::LIMIT));

                for ($j = 0; $j <= $counterPersonalPartners; $j++) {
                    $personalPartners = $user->getRepository()->getPersonalPartners(self::LIMIT, $j * self::LIMIT);
                    foreach ($personalPartners as $personalPartner) {
                        if ($personalPartner->firstPurchase) {
                            if (is_string($personalPartner->firstPurchase)) {
                                $firstPurchase = strtotime($personalPartner->firstPurchase);
                            } else {
                                $firstPurchase = $personalPartner->firstPurchase->toDateTime()->getTimestamp();
                            }
                            if ($firstPurchase > 0) {
                                $personalPartnersWithPurchases++;
                            }
                        }
                    }
                }

                $user->setAttribute('statistics.personalPartnersWithPurchases', $personalPartnersWithPurchases);

                $news = [];
                foreach ($user->system['readNews'] as $readNews) {
                    try {
                        $news[] = new ObjectID($readNews);
                    } catch (InvalidArgumentException $exception) {}
                }

                $unreadNews = News::raw()->count([
                    '_id' => ['$nin' => $news],
                    'lang' => $user->settings['selectedLang'],
                    'dateOfPublication' => ['$lte' => new UTCDateTime(time() * 1000)]
                ]);

                $user->setAttribute('statistics.unreadNews', $unreadNews);

                $promotions = [];
                foreach ($user->system['readPromotions'] as $readPromotion) {
                    try {
                        $promotions[] = new ObjectID($readPromotion);
                    } catch (InvalidArgumentException $exception) {}
                }

                $unreadPromotions = Promotion::raw()->count([
                    '_id' => ['$nin' => $promotions],
                    'lang' => $user->settings['selectedLang'],
                    'dateStart' => ['$lte' => new UTCDateTime(time() * 1000)],
                    'dateFinish' => ['$gte' => new UTCDateTime(time() * 1000)]
                ]);

                $user->setAttribute('statistics.unreadPromotions', $unreadPromotions);

                $user->leftSideNumberUsers = $user->getRepository()->countPartners(User::SIDE_LEFT);

                $user->rightSideNumberUsers = $user->getRepository()->countPartners(User::SIDE_RIGHT);

                $user->setAttribute('statistics.partnersWithPurchases', $user->getRepository()->countPartnersWithPurchases());

                $expirationDateBS = $user->expirationDateBS->toDateTime();
                $expirationDateBS = $expirationDateBS->setTime(23, 59, 59)->getTimestamp();
                $nowDate = Carbon::now()->timestamp;

                if ($user->bs && $expirationDateBS < $nowDate) {
                    $user->bs = false;
                }

                if (! $user->bs && $user->autoExtensionBS) {
                    $product = Product::where('product', '=', 4)->first();
                    if ($product && $user->moneys >= $product->price) {
                        $mainUser = User::where('username', '=', 'main')->first();
                        $comment = 'Auto extension BS ' . $user->username;
                        $transaction = Transaction::addMoneys(null, $user, $mainUser, $product->price, $comment);
                        if ($transaction) {
                            event(new MoneyAdded($transaction));
                            $sale = Sale::addSale($user, $product, Sale::PROJECT_BPT);
                            if ($sale) {
                                event(new SaleCreated($sale));
                            }
                        }
                    }
                }

                if ($user->save()) {
                    echo "\n END: " . gmdate('d.m.Y H:i:s', time()) . ' ' . $user->username . " OK\n";
                }
            }
        }
    }

}