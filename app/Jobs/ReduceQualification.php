<?php

namespace App\Jobs;

use App\Models\Settings;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ReduceQualification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $sponsors = User::where('qualification', '=', false)->where('statistics.pack', '>', 0)->get();

        $pointsSumToQualification = Settings::first()->pointsSumToQualification;

        foreach ($sponsors as $sponsor) {
            $leftQualification = false;
            $rightQualification = false;

            $users = User::where('sponsorId', '=', new \MongoDB\BSON\ObjectID($sponsor->_id))->get();

            foreach ($users as $user) {
                $totalLeft = Transaction::where('idFrom', '=', new \MongoDB\BSON\ObjectID($user->_id))
                    ->where('idTo', '=', new \MongoDB\BSON\ObjectID($sponsor->_id))
                    ->where('side', '=', Transaction::SIDE_LEFT)
                    ->where('type', '=', Transaction::TYPE_POINT)
                    ->sum('amount');

                $totalRight = Transaction::where('idFrom', '=', new \MongoDB\BSON\ObjectID($user->_id))
                    ->where('idTo', '=', new \MongoDB\BSON\ObjectID($sponsor->_id))
                    ->where('side', '=', Transaction::SIDE_RIGHT)
                    ->where('type', '=', Transaction::TYPE_POINT)
                    ->sum('amount');

                if ($totalLeft >= $pointsSumToQualification) {
                    $leftQualification = true;
                }

                if ($totalRight >= $pointsSumToQualification) {
                    $rightQualification = true;
                }

                if ($leftQualification && $rightQualification) {
                    break;
                }
            }

            if ($leftQualification && $rightQualification) {
                $sponsor->qualification = true;
                $sponsor->save();
            }
        }
    }

}