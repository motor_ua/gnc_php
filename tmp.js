/**
 * Регистрация пользователя.
 *
 * Вызов осуществляется POST запросом по адресу: https://<адрес сервера API>/user/
 * Формат POST запроса: https://<адрес сервера API>/user/
 *
 * Обязательные параметры:
 *  — sponsor 		: {type: String} — логин или номер счета пользователя (допустимы латинские буквы (a-z), цифры (0-9), дефис (-) и нижнее подчеркивание (_). «-»,«_» не могут находиться в начале и конце);
 * 	— username		: {type: String} — логин пользователя (допустимы латинские буквы (a-z), цифры (0-9), дефис (-) и нижнее подчеркивание (_). «-»,«_» не могут находиться в начале и конце);
 * 	— email			: {type: String} — email пользователя (допустимы латинские буквы (a-z), цифры (0-9), дефис (-) и нижнее подчеркивание (_). «-»,«_» не могут находиться в начале и конце);
 * 	— fname			: {type: String} — имя пользователя (допустимы любые буквы, цифры (0-9));
 * 	— sname			: {type: String} — фамилия пользователя (допустимы любые буквы, цифры (0-9));
 * 	— phone			: {type: Number} — номер телефона пользователя (допустимы цифры (0-9), номер телефона должен быть в полном формате);
 * 	— password 		: {type: String} — основной пароль (допустимы любые символы);
 * 	— finPassword 	: {type: String} — финансовый основной пароль (допустимы любые символы).
 *
 * Не обязательные параметры:
 * 	— skype 		: {type: String} — логин пользователя (допустимы латинские буквы (a-z), цифры (0-9), дефис (-) и нижнее подчеркивание (_));
 * 	— country 		: {type: String} – код страны пользователя в двухбуквенном формате ISO, по умолчанию «RU»;
 * 	— ip 			: {type: String} — ip пользователя;
 * 	— phoneViber 	: {type: String} — телефон Viber;
 * 	— phoneWhatsApp : {type: String} — телефон WhatsApp;
 * 	— phoneTelegram : {type: String} — телефон Telegram;
 * 	— phoneFB 		: {type: String} — телефон FB.
 *
 * Варианты ответа:
 * 	— В случае успеха, возвращает 200:
 * 	— В случае отказа авторизации, возвращает 403;
 * 	— В случае, если спонсор не найден, возвращает 404;
 * 	— В случае наличия пользователя с таким логином, возвращает 409;
 * 	— В случае ошибки в параметрах, возвращает 400;
 * 	— В случае ошибки сервера, возвращает 500.
 */

var async = require('async');
var mailManager = require('../../../libs/mailManager.js');
var location = require('../../../libs/location.js');

exports.post = function(req, res) {
    var _ip = req.headers['x-forwarded-for'] || req.headers['x-real-ip'] || req.connection.remoteAddress;

    if (authToAPI.check(_ip)) {
        var check = true;

        var az = /[a-zA-Z]/;
        var di = /[\d]/;
        var nd = /[\D]/;
        var un = /(^_|^-|\s|[!@#$%^&*()+|{}\\\/\[\]<>.,;:'"=@]|-$|_$)/;
        var em = /(.+@.+\..+)/;
        var rn = /(^-|[!@#$%^&*()+|{}\\\/\[\]<>.,;:'"=@_]|-$)/;
        var co = /([a-zA-Z]{3}|[^a-zA-Z])/;

        console.log(req.body);

        if (check && !req.body.phone) {
            check = false;
        }
        if (check && !req.body.sponsor) {
            check = false;
        }
        //TODO: сделать все проверки

        var skype = '';
        if (req.body.skype) {
            skype = req.body.skype;
        }

        var country = '';
        if (req.body.country) {
            country = req.body.country;
        }

        if (check) {

            //Регистрация.
            var u = {
                sponsor 	: req.body.sponsor,
                secondName  : req.body.sname,
                firstName   : req.body.fname,
                middleName  : '',
                email       : req.body.email.toLowerCase(),
                phone 		: req.body.phone,
                skype       : skype,
                username    : req.body.username.toLowerCase(),
                country 	: country.toLowerCase(),
                password    : req.body.password,
                finPassword : req.body.finPassword
            };

            if (req.body.phoneViber) {
                u.phoneViber = req.body.phoneViber;
            }
            if (req.body.phoneTelegram) {
                u.phoneTelegram = req.body.phoneTelegram;
            }
            if (req.body.phoneWhatsApp) {
                u.phoneWhatsApp = req.body.phoneWhatsApp;
            }
            if (req.body.phoneFB) {
                u.phoneFB = req.body.phoneFB;
            }

            DB.MONGO.model('user').registration(
                u,
                function(err, user) {
                    if(err) {
                        log.err(err.message);

                        switch (err.message) {
                            case 'Bad params':
                                res.sendStatus(404);
                                break;
                            case (function() {
                                if (err.message.indexOf('E11000 duplicate key error index') > -1) {
                                    return err.message;
                                } else {
                                    return ''
                                }
                            }()):
                                res.sendStatus(409);
                                break;
                            case 'Sponsor not found':
                                res.sendStatus(404);
                                break;
                            default: res.sendStatus(500);
                        }
                    } else {
                        log.notice('Registred new user', user.username);

                        if (req.body.ip) {
                            console.log(req.body.ip);
                            location.getJSON(req.body.ip, function(err, status, obj) {
                                if (err) {
                                    log.err(err);
                                } else {
                                    console.log(obj);
                                    DB.MONGO.model('settings').findOne(
                                        {},
                                        {'_id': 0, 'timeZones': {$elemMatch: {'utc': new RegExp('^' + obj.country.timezone + '$', "i")}}},
                                        function(err, settings) {
                                            if (err) {
                                                log.err(err);
                                            } else {
                                                if (settings.timeZones) {
                                                    console.log(settings.timeZones);
                                                    user.settings.timeZone = settings.timeZones[0];

                                                    user.save(function(err) {
                                                        if (err) {
                                                            log.err(err);
                                                        }
                                                    });
                                                }
                                            }
                                        }
                                    );
                                }
                            });
                        }

                        DB.MONGO.model('transaction').addStocks({
                            idFrom      : gUserMain._id,
                            idTo        : user._id,
                            amount      : 100,
                            forWhat     : 'From registration',
                            saldoFrom   : 0,
                            saldoTo     : 0
                        }, function(err, transaction) {
                            if (err) {
                                log.err(err);
                            }
                        });

                        mailManager.sendRegistration(user._id, function(err) {
                            if (err) {
                                log.err(err);
                            } else {
                                log.debug('Registration mail for user ' + user.username + ' send');
                            }
                        });

                        res.status(200).send(user);
                    }
                }
            );

        } else {
            res.sendStatus(400);
        }
    } else {
        res.sendStatus(403);
    }
}