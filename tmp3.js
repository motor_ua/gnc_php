var async = require('async');
var mailManager = require('../../../libs/mailManager.js');

exports.post = function(req, res) {
    var sponsorPhone = req.body.sponsor_phone;
    var phone = req.body.phone;
    var email = req.body.email;
    var country = req.body.country;
    var password = req.body.password;

    if (sponsorPhone && phone && email && country && password) {
        if (sponsorPhone[0] == '+') {
            sponsorPhone = sponsorPhone.substring(1);
        }
        if (phone[0] == '+') {
            phone = phone.substring(1);
        }
        async.waterfall([
            function(callback) {
                DB.MONGO.model('user').findOne(
                    {$or: [
                        {'phoneNumber': phone},
                        {'phoneNumber2': phone},
                        {'phoneWellness': phone},
                        {'phoneNumber': '+' + phone},
                        {'phoneNumber2': '+' + phone},
                        {'phoneWellness': '+' + phone},
                        {'email': email}
                    ]},
                    function (err, user) {
                        if (err) {
                            callback(err);
                        } else {
                            if (user) {
                                res.sendStatus(409);
                            } else {
                                callback(null);
                            }
                        }
                    }
                );
            },
            function(callback) {
                DB.MONGO.model('user').findOne(
                    {$or: [
                        {'phoneNumber': sponsorPhone},
                        {'phoneNumber2': sponsorPhone},
                        {'phoneWellness': sponsorPhone},
                        {'phoneNumber': '+' + sponsorPhone},
                        {'phoneNumber2': '+' + sponsorPhone},
                        {'phoneWellness': '+' + sponsorPhone}
                    ]},
                    function (err, sponsor) {
                        if (err) {
                            callback(err);
                        } else {
                            if (sponsor) {
                                callback(null, sponsor);
                            } else {
                                res.sendStatus(404);
                            }
                        }
                    }
                );
            },
            function(sponsor, callback) {
                DB.MONGO.model('user').registration(
                    {
                        sponsor 	: sponsor.username,
                        email       : email.toLowerCase(),
                        phone 		: '+' + phone,
                        country 	: country.toLowerCase(),
                        password    : password
                    },
                    function(err, user) {
                        if (err) {
                            console.log(err);
                            switch (err.message) {
                                case 'Bad params':
                                    res.sendStatus(404);
                                    break;
                                case 'Sponsor not found':
                                    res.sendStatus(404);
                                    break;
                                default: res.sendStatus(500);
                            }
                        } else {
                            DB.MONGO.model('transaction').addStocks({
                                idFrom      : gUserMain._id,
                                idTo        : user._id,
                                amount      : 100,
                                forWhat     : 'From registration',
                                saldoFrom   : 0,
                                saldoTo     : 0
                            }, function(err, transaction) {
                                if (err) {
                                    callback(err);
                                }
                            });

                            mailManager.sendRegistration(user._id, function(err) {
                                if (err) {
                                    callback(err);
                                } else {
                                    log.debug('Registration mail for user ' + user.email + ' send');
                                }
                            });

                            callback(null, user);
                        }
                    }
                );
            }
        ], function(err, user) {
            if (err) {
                callback(err);
            } else {
                res.status(200).send(user);
            }
        });
    } else {
        res.sendStatus(400);
    }

}