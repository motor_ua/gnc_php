<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => []], function () {

    Route::post('/authenticate', 'Api\AuthController@authenticate');

    Route::post('/sales', 'Api\SaleController@createSale');
    Route::delete('/sales', 'Api\SaleController@cancelSale');

    Route::post('/user', 'Api\UserController@register');
    Route::post('/user/mobileRegistration', 'Api\UserController@mobileRegister');
    Route::get('/user/{param?}', 'Api\UserController@userInfo');

});


