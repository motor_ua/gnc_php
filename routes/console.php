<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('users:closeSteps', function () {
    dispatch(new App\Jobs\CloseSteps());
});

Artisan::command('users:reduceQualification', function () {
    dispatch(new App\Jobs\ReduceQualification());
});

Artisan::command('users:refresh', function () {
    dispatch(new App\Jobs\RefreshUserData());
});

Artisan::command('users:notify', function () {
    dispatch(new App\Jobs\Notification());
});

Artisan::command('users:setMentorBonus', function () {
    dispatch(new App\Jobs\MentorBonus());
});

Artisan::command('users:setExecutiveBonus', function () {
    dispatch(new App\Jobs\ExecutiveBonus());
});
